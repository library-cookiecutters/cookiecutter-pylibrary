git status

if [ -z ${sphinx_docs_hosting+ABC} ]; then
  TOP_LEVEL_NAMESPACE=$(echo $NAMESPACE_COOKIECUT_TO | cut -d '/' -f 1)
  REST_OF_NAMESPACE=${NAMESPACE_COOKIECUT_TO/$TOP_LEVEL_NAMESPACE}
  export sphinx_docs_hosting=http://$TOP_LEVEL_NAMESPACE.${CI_PAGES_DOMAIN}${REST_OF_NAMESPACE}/$(echo $repo_name | tr '[:upper:]' '[:lower:]')
fi
printf "  sphinx_docs_hosting:" >> $HOME/.cookiecutterrc
echo " $sphinx_docs_hosting" >> $HOME/.cookiecutterrc
printf "  email:" >> $HOME/.cookiecutterrc
echo " $GITLAB_USER_EMAIL" >> $HOME/.cookiecutterrc
printf "  full_name:" >> $HOME/.cookiecutterrc
echo " $GITLAB_USER_NAME" >> $HOME/.cookiecutterrc
printf "  CI_SERVER_HOST:" >> $HOME/.cookiecutterrc
echo " $CI_SERVER_HOST" >> $HOME/.cookiecutterrc

cat add.to.cookiecutterrc >> $HOME/.cookiecutterrc
cat $HOME/.cookiecutterrc
apk add gcc musl-dev
# cookiepatcher automatically reads from the .cookiecutterrc in the target repo.
cookiepatcher ./ $repo_name --no-input
sed --in-place "s/fkrull\/multi-python:bionic/python:alpine/" $repo_name/.gitlab-ci.yml
if ls $repo_name/.flake8; then echo "flake8"; else
echo "[flake8]" > $repo_name/.flake8
echo "extend-ignore = E111,E114" >> $repo_name/.flake8
fi
# always overwrote MANIFEST.in if have MANIFEST.in
if ls $repo_name/MANIFEST.in; then
echo "include .flake8" >> $repo_name/MANIFEST.in
fi
